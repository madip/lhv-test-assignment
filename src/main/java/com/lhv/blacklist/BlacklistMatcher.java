package com.lhv.blacklist;

import org.apache.commons.text.similarity.JaroWinklerSimilarity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * <p>BlacklistMatcher class.</p>
 *
 * @author madis
 * @version $Id: $Id
 */
public class BlacklistMatcher {

    /**
     * <p>Given the searchable name, blacklist stream and noise words stream checkForNameMatches
     * method will return collection of matching names. If no matches found empty collection is returned.
     * </p>
     *
     * @param name                   a {@link java.lang.String} object.
     * @param blacklistedNamesStream a {@link java.util.function.Supplier} object.
     * @param noiseWordsStream       a {@link java.util.function.Supplier} object.
     * @return a {@link java.util.Collection} object.
     */
    public Collection<String> checkForNameMatches(
            String name,
            Supplier<Stream<String>> blacklistedNamesStream,
            Supplier<Stream<String>> noiseWordsStream
    ) {
        String nameWithoutSpecialCharacters = name.replaceAll("[^a-zA-Z0-9]", " ");

        ArrayList<String> nameArrayWitOutNoiseWords =
                Stream.of(nameWithoutSpecialCharacters.toLowerCase().split(" "))
                        .collect(Collectors.toCollection(ArrayList<String>::new));
        nameArrayWitOutNoiseWords.removeAll(noiseWordsStream.get().collect(Collectors.toList()));

        String filteredName = String.join(" ", nameArrayWitOutNoiseWords);
        return blacklistedNamesStream
                .get()
                .filter(blackListName -> {
                            if (name.contains(".")) {
                                return checkForAbbreviation(name, blackListName) >= 0.83;
                            } else {
                                return checkForJaroWinklerSimilarity(filteredName, blackListName.toLowerCase()) > 0.9 ||
                                        findLevenshteinSimilar(filteredName, blackListName).size() > 0 ||
                                        sortAndCompareStringChars(filteredName, blackListName.toLowerCase()) == 0;
                            }
                        }
                )
                .collect(Collectors.toList());
    }

    /**
     * <p>Searches the given collection of strings and returns a collection of strings similar to a given string</p>
     *
     * @param filteredName  a {@link java.lang.String} object.
     * @param blackListName a {@link java.lang.String} object.
     * @return collection.
     */
    private Collection<String> findLevenshteinSimilar(String filteredName, String blackListName) {
        return Levenshtein.findSimilar(
                Collections.singletonList(filteredName),
                blackListName.toLowerCase(),
                3,
                Math.min(blackListName.toLowerCase().length() - 1, 4)
        );
    }

    private double checkForAbbreviation(String name, String blackListName) {
        return checkForJaroWinklerSimilarity(name, blackListName);
    }

    /**
     * <p>A similarity algorithm indicating the percentage of matched characters between two character sequences.</p>
     *
     * @param name          a {@link java.lang.String} object.
     * @param blackListName a {@link java.lang.String} object.
     * @return a double.
     */
    public double checkForJaroWinklerSimilarity(String name, String blackListName) {
        return new JaroWinklerSimilarity().apply(name, blackListName);
    }

    /**
     * <p>Method removes spaces from two comparable strings and makes them lower case.
     * Then the strings are sorted and compared. Comparison result is returned.
     * </p>
     *
     * @param name          a {@link java.lang.String} object.
     * @param blackListName a {@link java.lang.String} object.
     * @return a int.
     */
    private int sortAndCompareStringChars(String name, String blackListName) {
        String stringA = getStringWithSortedChars(name);
        String stringB = getStringWithSortedChars(blackListName);
        return stringA.compareTo(stringB);
    }

    private String getStringWithSortedChars(String name) {
        char[] nameChars = name.replaceAll("\\s+", "").toCharArray();
        Arrays.sort(nameChars);
        return new String(nameChars);
    }
}
