package com.lhv.blacklist;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Scanner;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * <p>BlacklistApplication class.</p>
 *
 * @author madis
 * @version $Id: $Id
 */
public class BlacklistApplication {

    /**
     * <p>Application main.</p>
     *
     * @param args an array of {@link java.lang.String} objects.
     */
    public static void main(String[] args) {
        BlacklistApplication blacklistApplication = new BlacklistApplication();
        blacklistApplication.launch();
    }

    /**
     * <p>Application launches looping for user input from this method.</p>
     */
    public void launch() {
        Supplier<Stream<String>> blacklistedNamesSupplier = getFileSupplier("blacklisted_names.txt");
        Supplier<Stream<String>> noiseWordsSupplier = getFileSupplier("noise_words.txt");

        Scanner userInputName = new Scanner(System.in);
        System.out.println("Blacklist name search application.");
        System.out.println("To exit application type 'exit'.");

        BlacklistMatcher blacklistMatcher = new BlacklistMatcher();
        while (true) {
            System.out.println("Insert name for black list search:");
            String searchName = userInputName.nextLine();
            if (searchName.toLowerCase().equals("exit")) {
                break;
            }
            if (!searchName.isEmpty()) {
                Collection<String> matchedNames =
                        blacklistMatcher.checkForNameMatches(searchName, blacklistedNamesSupplier, noiseWordsSupplier);
                if (matchedNames.isEmpty()) {
                    System.out.println("No matches found");
                } else {
                    System.out.println("Match(es) found");
                    System.out.println(matchedNames);
                }
            }
        }
    }

    /**
     * <p>getFileStreamSupplier is a method that gets input filename content from resources.</p>
     *
     * @param filename a {@link java.lang.String} object.
     * @return a {@link java.util.function.Supplier} object.
     */
    public Supplier<Stream<String>> getFileSupplier(String filename) {
        return () -> {
            InputStream fileInputStream = this.getFileFromResources(filename);
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, StandardCharsets.UTF_8);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            return bufferedReader.lines();
        };
    }

    private InputStream getFileFromResources(String fileName) {
        ClassLoader classLoader = getClass().getClassLoader();
        return classLoader.getResourceAsStream(fileName);
    }
}
