package com.lhv.blacklist;

import org.junit.Test;

import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

/**
 * <p>SimpleBlacklistMatchingTest class.</p>
 *
 * @author madis
 * @version $Id: $Id
 * @since 0.0.1
 */
public class SimpleBlacklistMatchingTest {

    /**
     * <p>blacklistMatcherTest.</p>
     */
    @Test
    public void blacklistMatcherTest() {
        BlacklistMatcher blacklistMatcher = new BlacklistMatcher();
        BlacklistApplication blacklistApplication = new BlacklistApplication();
        Supplier<Stream<String>> blacklistedNamesStream = blacklistApplication.getFileSupplier("blacklisted_names.txt");
        Supplier<Stream<String>> noiseWordsStream = blacklistApplication.getFileSupplier("noise_words.txt");

        List<String> expected = Collections.singletonList("Osama Bin Laden");
        assertEquals(expected, blacklistMatcher.checkForNameMatches("Osama Laden", blacklistedNamesStream, noiseWordsStream));
        assertEquals(expected, blacklistMatcher.checkForNameMatches("Osama Bin Laden", blacklistedNamesStream, noiseWordsStream));
        assertEquals(expected, blacklistMatcher.checkForNameMatches("Bin Laden, Osama", blacklistedNamesStream, noiseWordsStream));
        assertEquals(expected, blacklistMatcher.checkForNameMatches("Laden Osama Bin", blacklistedNamesStream, noiseWordsStream));
        assertEquals(expected, blacklistMatcher.checkForNameMatches("to the osama bin laden", blacklistedNamesStream, noiseWordsStream));
        assertEquals(expected, blacklistMatcher.checkForNameMatches("osama and bin laden", blacklistedNamesStream, noiseWordsStream));

        assertEquals(Collections.singletonList("Madis Peep"), blacklistMatcher.checkForNameMatches("madus peen", blacklistedNamesStream, noiseWordsStream));
        assertEquals(Collections.singletonList("Robert"), blacklistMatcher.checkForNameMatches("Bert", blacklistedNamesStream, noiseWordsStream));
        assertEquals(Collections.singletonList("Cairns"), blacklistMatcher.checkForNameMatches("Kearns", blacklistedNamesStream, noiseWordsStream));
        assertEquals(Collections.singletonList("Cairns"), blacklistMatcher.checkForNameMatches("Kerns", blacklistedNamesStream, noiseWordsStream));
        assertEquals(Collections.singletonList("Dona Oliver"), blacklistMatcher.checkForNameMatches("Oliver Dona", blacklistedNamesStream, noiseWordsStream));

        assertEquals(Collections.singletonList("Joe Luis Webb"), blacklistMatcher.checkForNameMatches("Joe L. Webb", blacklistedNamesStream, noiseWordsStream));
        assertEquals(Collections.singletonList("Abdul Rahman Yasin"), blacklistMatcher.checkForNameMatches("Abdul R. Yasin", blacklistedNamesStream, noiseWordsStream));

        assertEquals(Collections.singletonList("Barbara"), blacklistMatcher.checkForNameMatches("Barb.", blacklistedNamesStream, noiseWordsStream));
        assertEquals(Collections.singletonList("Bartholomew"), blacklistMatcher.checkForNameMatches("Bart.", blacklistedNamesStream, noiseWordsStream));
        assertEquals(Collections.singletonList("Benjamin"), blacklistMatcher.checkForNameMatches("Benj.", blacklistedNamesStream, noiseWordsStream));
        assertEquals(Collections.singletonList("Bridget"), blacklistMatcher.checkForNameMatches("Brid.", blacklistedNamesStream, noiseWordsStream));
        assertEquals(Collections.singletonList("Catherine"), blacklistMatcher.checkForNameMatches("Cath.", blacklistedNamesStream, noiseWordsStream));
    }

    /**
     * <p>compareStringsTest.</p>
     */
    @Test
    public void compareStringsTest() {
        String searchString = "Osama Bin Laden";
        BlacklistMatcher blacklistMatcher = new BlacklistMatcher();
        assertEquals(blacklistMatcher.checkForJaroWinklerSimilarity(searchString, "Osama Laden"), 0.9, 0.01);
        assertEquals(blacklistMatcher.checkForJaroWinklerSimilarity(searchString, "Osama Bin Laden"), 1, 0.01);
        assertEquals(blacklistMatcher.checkForJaroWinklerSimilarity(searchString, "Bin Laden, Osama"), 0.64, 0.01);
        assertEquals(blacklistMatcher.checkForJaroWinklerSimilarity(searchString, "Laden Osama Bin"), 0.71, 0.01);
        assertEquals(blacklistMatcher.checkForJaroWinklerSimilarity(searchString, "to the osama bin laden"), 0.62, 0.01);
        assertEquals(blacklistMatcher.checkForJaroWinklerSimilarity(searchString, "osama and bin laden"), 0.72, 0.01);

        assertEquals(blacklistMatcher.checkForJaroWinklerSimilarity("Joe Luis Webb", "Joe L. Webb"), 0.93, 0.01);
        assertEquals(blacklistMatcher.checkForJaroWinklerSimilarity("Augustus", "Aug."), 0.79, 0.01);
        assertEquals(blacklistMatcher.checkForJaroWinklerSimilarity("Augustus", "Aug"), 0.85, 0.01);
        assertEquals(blacklistMatcher.checkForJaroWinklerSimilarity("Barbara", "Barb."), 0.87, 0.01);
        assertEquals(blacklistMatcher.checkForJaroWinklerSimilarity("Barbara Mora", "Barb."), 0.82, 0.01);
        assertEquals(blacklistMatcher.checkForJaroWinklerSimilarity("Bartholomew", "Bart."), 0.83, 0.01);
        assertEquals(blacklistMatcher.checkForJaroWinklerSimilarity("Abdul Rahman Yasin", "Abdul R. Yasin"), 0.88, 0.01);
        assertEquals(blacklistMatcher.checkForJaroWinklerSimilarity("Benjamin", "Benj"), 0.9, 0.01);
        assertEquals(blacklistMatcher.checkForJaroWinklerSimilarity("Bridget", "Brid"), 0.91, 0.01);
        assertEquals(blacklistMatcher.checkForJaroWinklerSimilarity("Catherine", "Cath"), 0.88, 0.01);
        assertEquals(blacklistMatcher.checkForJaroWinklerSimilarity("Charles", "Chas."), 0.67, 0.01);
        assertEquals(blacklistMatcher.checkForJaroWinklerSimilarity("Chas Silva", "Chas."), 0.84, 0.01);
    }
}
