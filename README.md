# LHV - Blacklist Name Matching #

#### Background ####
Banks are responsible for stopping money laundering. Hence it is necessary to avoid money transfers to terrorists and criminals.

#### Task ####
Task was to implement simple algorithm to compare given name against blacklist to detect such transfers.
In case of partial match, algorithm should return as few "false positive" matches as possible.
Implement in any language that you feel most comfortable in.

### How to run project? ###

* Clone the project to your PC
* Open project directory in console
* Execute ```mvn clean install && java -jar target/LHV-0.0.1-SNAPSHOT.jar```
* Insert name that you want to check against blacklist
* To exit the application type and enter "exit"

### Solution ###

Solution is using java 11 and maven. Solution is a console application that will return
collection of matches according to user name input. Blacklist names are located in resources "blacklisted_names.txt"
and noise words that are removed from search are located in "noise_words.txt".

##### Algorithms: #####
* Levenshtein - Searches the given collection of strings and returns a collection of strings similar to a given string. Uses reasonable default
              values for human-readable strings. The returned collection will be sorted according to their similarity with the string with the best
              match at the first position. 

* JaroWinklerSimilarity - A similarity algorithm indicating the percentage of matched characters between two character sequences.

### Future improvements ###
If application user company is not willing to pay for existing good solutions then in order
for this application to work better and be more scalable list of changes need to be done:

* Add API endpoint so other applications can use it as well
* Add more algorithms
* Move from file based blacklist and noise file to some database solution
* Add UI for easier blacklist management
* All hardcoded properties should be in properties file
